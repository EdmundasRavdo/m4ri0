package mario;

import java.awt.Graphics;

public interface Npc {
	   void render(Graphics g);
	   void tick();
	}