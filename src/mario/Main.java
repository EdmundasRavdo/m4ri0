package mario;

import javax.swing.JFrame;

public class Main {
	
	static String TITLE = "MARIO";
	
	public static void main(String[] args) {
		Game game = Game.getInstance();
		JFrame frame = new JFrame(TITLE);
		frame.add(game);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		game.start();
	}
}


