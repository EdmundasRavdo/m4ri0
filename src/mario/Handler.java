package mario;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import mario.entity.Coin;
import mario.entity.Entity;
import mario.entity.mob.Goomba;
import mario.entity.mob.Player;
import mario.entity.mob.TowerBoss;
import mario.entity.powerup.Mushroom;
import mario.tile.Pipe;
import mario.tile.PowerUpBlock;
import mario.tile.Tile;
import mario.tile.Wall;

public class Handler {
	public LinkedList<Entity> entity = new LinkedList<Entity>();
	public LinkedList<Tile> tile = new LinkedList<Tile>();
	NpcFactory npcFactory = new NpcFactory();
	//Goomba npc1 = npcFactory.getNpc("goo", this);
	
	// Handler() {
	// createLevel();
	// }

	public void render(Graphics g) {
		// for(Entity en:entity) {
		// en.render(g);
		// }
		//
		// for(Tile ti:tile) {
		// ti.render(g);
		// }
		
//		npc1.render(g);
//		npc1.tick();
		for (int i = 0; i < entity.size(); i++) {
			Entity en = entity.get(i);
			en.render(g);
		}

		for (int i = 0; i < tile.size(); i++) {
			Tile ti = tile.get(i);
			ti.render(g);
		}
	}

	public void tick() {
		// for(Entity en:entity) {
		// en.tick();
		// }
		//
		// for(Tile ti:tile) {
		// ti.tick();
		// }

		for (int i = 0; i < entity.size(); i++) {
			Entity en = entity.get(i);
			
			en.tick();
			
		}

		for (int i = 0; i < tile.size(); i++) {
			Tile ti = tile.get(i);
			ti.tick();
		}

	}

	public void addEntity(Entity en) {
		entity.add(en);
	}
	
	public void removeEntity(Entity en) {
		entity.remove(en);
	}

	public void addTile(Tile ti) {
		tile.add(ti);
	}

	public void removeTile(Tile ti) {
		tile.remove(ti);
	}

	public void createLevel(BufferedImage level) {
		int width = level.getWidth();
		int height = level.getHeight();

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int pixel = level.getRGB(x, y);

				int red = (pixel >> 16) & 0xff;
				int green = (pixel >> 8) & 0xff;
				int blue = (pixel) & 0xff;

				if (red == 0 && green == 0 && blue == 0) {
					addTile(new Wall(x * 64, y * 64, 64, 64, true, Id.wall, this));
				}
				if (red == 0 && green == 0 && blue == 255) {
					addEntity(new Player(x * 64, y * 64, 48, 48, Id.player, this));
				}
				if (red == 255 && green == 0 && blue == 0) {
					addEntity(new Mushroom(x * 64, y * 64, 64, 64, Id.mushroom, this));
				}
				if (red == 100 && green == 100 && blue == 0) { // Pasirinkti
																// spalva
					//addEntity(new Goomba(x * 64, y * 64, 64, 64, Id.goomba, this));
					addEntity(npcFactory.getNpc("goo", this, x * 64, y * 64));
				}
				if (red == 255 && green == 255 && blue == 0) { // Pasirinkti
																// spalva
					addTile(new PowerUpBlock(x * 64, y * 64, 64, 64, true, Id.powerUp, this, Game.mushroom));
				}
				if (red == 0 && (green > 123 && green < 129) && blue == 0) { // Pasirinkti
																				// spalva
					addTile(new Pipe(x * 64, y * 64, 64, 64 * 15, true, Id.pipe, this, 128 - green));
				}
				if (red == 80 && green == 0 && blue == 0) { // Pasirinkti spalva
					addEntity(new Coin(x * 64, y * 64, 64, 64, Id.coin, this));
				}
				if (red == 255 && green == 150 && blue == 200) { // Pasirinkti
																	// spalva
					addEntity(new TowerBoss(x * 64, y * 64, 64, 64, Id.towerBoss, this, 3));
				}
			}
		}
	}

	public void clearLevel() {
		entity.clear();
		tile.clear();
	}

	// public void createLevel() {
	// for(int i=0; i<Game.WIDTH*Game.SCALE/64+1;i++) {
	// addTile(new
	// Wall(i*64,Game.HEIGHT*Game.SCALE-64,64,64,true,Id.wall,this));
	// if(i!=0 && i!=1 && i!=8 && i!=7) {addTile(new
	// Wall(i*64,300,64,64,true,Id.wall,this));}
	// }
	// }

}
