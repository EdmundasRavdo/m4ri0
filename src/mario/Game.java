package mario;

import javax.imageio.ImageIO;

import mario.entity.Coin;
import mario.entity.Entity;
import mario.gfx.Sprite;
import mario.gfx.SpriteSheet;
import mario.input.KeyInput;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;

public final class Game extends Canvas implements Runnable {

	public static final int WIDTH = 320; // 270
	public static final int HEIGHT = 180; // WIDTH/14*10;
	public static final int SCALE = 4;
	public static final String TITLE = "Mario";

	private Thread thread;
	private boolean running = false;
	private BufferedImage image;

	public static int coins = 0;
	public static int lives = 5;
	public static int deathScreenTime = 0;

	public static boolean showDeathScreen = true;
	public static boolean gameOver = false;

	public static Handler handler;
	public static SpriteSheet sheet;
	public static Camera cam;

	public static Sprite grass;

	public static Sprite powerUp;
	public static Sprite usedPowerUp;

	public static Sprite[] player;
	public static Sprite mushroom;
	public static Sprite coin;
	public static Sprite[] goomba;

	private static Game instance;

	private Game() {
		Dimension size = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
		setPreferredSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
	}

	private void init() {
		handler = new Handler();
		sheet = new SpriteSheet("/spritesheet2.png");
		cam = new Camera();

		addKeyListener(new KeyInput());

		grass = new Sprite(sheet, 1, 1);

		powerUp = new Sprite(sheet, 3, 1);
		usedPowerUp = new Sprite(sheet, 3, 2);
		coin = new Sprite(sheet, 4, 1);

		player = new Sprite[10];
		mushroom = new Sprite(sheet, 2, 1);
		goomba = new Sprite[10];

		for (int i = 0; i < player.length; i++) {
			player[i] = new Sprite(sheet, i + 1, 3);
		}

		for (int i = 0; i < goomba.length; i++) {
			goomba[i] = new Sprite(sheet, i + 1, 4);
		}

		handler.addEntity(new Coin(200, 100, 64, 64, Id.coin, handler));
		// handler.createLevel();
		// handler.addEntity(new Player(300, 512, 64, 64, true,
		// Id.player,handler));
		//// handler.addEntity(new Player(305, 512, 64, 64, true,
		// Id.player,handler));
		// handler.addEntity(new
		// Mushroom(200,200,64,64,true,Id.mushroom,handler));
		// handler.addEntity(new Goomba(300,200,64,64,true,Id.goomba,handler));

		try {
			image = ImageIO.read(getClass().getResource("/level.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public synchronized void start() {
		if (running) {
			return;
		}
		running = true;
		thread = new Thread(this, "Thread");
		thread.start();
	}

	private synchronized void stop() {
		if (!running) {
			return;
		}
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public void run() {
		init();
		requestFocus();
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		double delta = 0.0;
		double ns = 1000000000.0 / 60.0;
		int frames = 0;
		int ticks = 0;
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			// while(delta>=1) {
			// tick();
			ticks++;
			// delta--;
			// }
			if (System.currentTimeMillis() - timer > 20) {
				render();
				tick();
			}
			// render();
			// tick();
			frames++;
			if (System.currentTimeMillis() - timer > 2000) {// tvarkyt -didint
				timer += 400;// tvarkyt -mazint
				System.out.println(frames + " Frames per second, " + ticks + " updates per second");
				frames = 0;
				ticks = 0;
			}
		}
		stop();
	}

	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			return;
		}
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		if (!showDeathScreen) {
			g.drawImage(coin.getBufferedImage(), 20, 20, 75, 75, null);
			g.setColor(Color.WHITE);
			g.setFont(new Font("Courier", Font.BOLD, 20));
			g.drawString("x" + coins, 130, 95);
		}
		if (showDeathScreen) {
			if (!gameOver) {
				g.setColor(Color.WHITE);
				g.setFont(new Font("Courier", Font.BOLD, 50));
				g.drawImage(Game.player[0].getBufferedImage(), 500, 300, 100, 100, null);
				g.drawString("x" + lives, 610, 400);
			} else {
				g.setColor(Color.WHITE); //
				g.setFont(new Font("Courier", Font.BOLD, 50));
				g.drawString("Game over :(" + lives, 610, 400);
			}

		}
		g.translate(cam.getX(), cam.getY());
		if (!showDeathScreen)
			handler.render(g);
		g.dispose();
		bs.show();
	}

	public void tick() {
		handler.tick();

		for (int i = 0; i < handler.entity.size(); i++) {
			Entity e = handler.entity.get(i);
			if (e.getId() == Id.player) {
				if (!e.goingDownPipe) { //
					cam.tick(e);
				} //

			}

		}
		if (showDeathScreen && !gameOver) {
			deathScreenTime++;
		}
		if (deathScreenTime >= 180) {
			showDeathScreen = false;
			deathScreenTime = 0;
			handler.clearLevel();
			handler.createLevel(image);
		}

	}

	public static Game getInstance() {
		if (instance == null) {
			instance = new Game();
		}
		return instance;
	}

}
