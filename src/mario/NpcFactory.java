package mario;

import mario.entity.mob.Goomba;

public class NpcFactory {
	
	   //use getShape method to get object of type shape 
	   public Goomba getNpc(String npcType, Handler handler, int x, int y){
	      if(npcType == null){
	         return null;
	      }		
	      if(npcType.equalsIgnoreCase("goo")){
	         return new Goomba(x, y, 64, 64, Id.goomba, handler);
	         
	      } 
	      
	      return null;
	   }
	}